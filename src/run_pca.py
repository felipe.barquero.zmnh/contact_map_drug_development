## Packages Requiered the Pseudolikelihood Maximition  PlmDCA 
from pydca.plmdca import plmdca
from pydca.meanfield_dca import meanfield_dca
from pydca.sequence_backmapper import sequence_backmapper
from pydca.msa_trimmer import msa_trimmer
from pydca.contact_visualizer import contact_visualizer
from pydca.dca_utilities import dca_utilities


##### Import Proteins with Pfams.

import pandas as pd
df = pd.read_csv('../data/proteins_with_pfams.csv')


##### Utils and Libraries Required
import numpy as np
import requests
import shutil
import os
import gzip
from Bio import Entrez
from Bio import SeqIO
from Bio.Emboss.Applications import SeqretCommandline

##### Gathers the Stockholm file for all Pfams.

downloaded_pair = {}
downloaded_pfams = []
downloaded_proteins = []
#print(df.iloc[10].pfam_list.split("'")[1])

##### Function to extract .stockholm.gz
def unzip_pfam_gz(input_file,output_file):
    try:
        # Open the input file in binary mode and read its contents
        with gzip.open(input_file, 'rb') as f_in:
            # Open the output file in binary mode and write the uncompressed contents
            with open(output_file, 'wb') as f_out:
                shutil.copyfileobj(f_in, f_out)
        os.remove(input_file)
    except:
        print("Unzip Unsuccessful ----- :( ------")


def compute_dca(pfam, ref,pfam_id ,protein_id):
    
    # create MSATrimmer instance 
    trimmer = msa_trimmer.MSATrimmer(
        pfam, biomolecule='protein', 
        refseq_file=ref,
    )
    trimmed_data = trimmer.get_msa_trimmed_by_refseq(remove_all_gaps=True)

    #write trimmed msa to file in FASTA format
    trimmed_data_outfile = '../data/pfams/'+pfam_id +'_Trimmed.fa'
    with open(trimmed_data_outfile, 'w') as fh:
        for seqid, seq in trimmed_data:
            fh.write('>{}\n{}\n'.format(seqid, seq))

    # Compute DCA scores using Pseudolikelihood maximization algorithm

    plmdca_inst = plmdca.PlmDCA(
        trimmed_data_outfile,
        'protein',
        seqid = 0.8,
        lambda_h = 1.0,
        lambda_J = 20.0,
        num_threads = 10,
        max_iterations = 500,
    )

    # compute DCA scores summarized by Frobenius norm and average product corrected
    plmdca_FN_APC = plmdca_inst.compute_sorted_FN_APC()
    arr_plmdca = np.array(plmdca_FN_APC)
    print(arr_plmdca)
    print("plmdca Computed ____________--")
    with open('../data/contact-maps/' +protein_id+'__' + pfam_id +'__dca__.npy', 'wb') as fp:
        np.save(fp,arr_plmdca)
        print('Done')
    
    print("plmdca Stored ____________--")

    #for site_pair, score in plmdca_FN_APC[:5]:
    #   print(site_pair, score)



    #create mean-field DCA instance 
    mfdca_inst = meanfield_dca.MeanFieldDCA(
        trimmed_data_outfile,
        'protein',
        pseudocount = 0.5,
        seqid = 0.8,

    )

    # Compute average product corrected Frobenius norm of the couplings
    mfdca_FN_APC = mfdca_inst.compute_sorted_FN_APC()
    arr_mfdca = np.array(mfdca_FN_APC)
    print("mfdca  Computed ____________--")
    with open('../data/contact-maps/' +protein_id+'__' + pfam_id +'__mfdca__.npy', 'wb') as fp:
        np.save(fp,arr_mfdca)
        print('Done')
    
    print("mfdca Stored ____________--")

    

    




for i in range(len(df)):
    # Set the UniProt ID of the protein
    uniprot_id = df.iloc[i].uniprot_id


    # Use Entrez to retrieve the protein record from UniProt
    handle = Entrez.efetch(db="protein", id=uniprot_id, rettype="fasta", retmode="text")
    record = SeqIO.read(handle, "fasta")


    # Save the protein sequence as a FASTA file
    output_file = '../data/proteins/'+uniprot_id + ".fasta"
    with open(output_file, "w") as f:
        SeqIO.write(record, f, "fasta")

    # Set the Pfam ID of the first family in the list 
    try:
        pfam_id = df.iloc[i].pfam_list.split("'")[1]
        
        downloaded_pair[uniprot_id]= pfam_id 
        if pfam_id not in downloaded_pfams:
            # Set the URL of the file to download
            url = "https://www.ebi.ac.uk/interpro/wwwapi/entry/pfam/"+pfam_id +"/?annotation=alignment:full&filename=fastq&download"
        
            # Set the path and name of the output file
            output_file = "../data/pfams/"+pfam_id +".stockholm.gz"

            # Download the file and save it to the output file
            response = requests.get(url, stream=True)
            with open(output_file, "wb") as f:
                shutil.copyfileobj(response.raw, f)

            unzip_pfam_gz("../data/pfams/"+pfam_id +".stockholm.gz","../data/pfams/"+pfam_id +".ann")



            # Print a success message if the file was downloaded and saved successfully
            if os.path.isfile("../data/pfams/"+pfam_id +".ann"):

                print("File downloaded and saved as " + output_file)
                downloaded_pfams.append(pfam_id)

                # Input file name and format
                input_file = "../data/pfams/"+pfam_id +".ann"
                input_format = "stockholm"

                # Output file name and format
                output_file =  "../data/pfams/"+pfam_id +".fasta"
                output_format = "fasta"

                # Run seqret to convert the input file to the output format
                seqret_cline = SeqretCommandline(sequence=input_file, outseq=output_file, sformat=input_format, osformat=output_format)
                stdout, stderr = seqret_cline()

                # Check for errors
                
                
                print("Conversion complete")
                os.remove(input_file)
                compute_dca("../data/pfams/"+pfam_id +".fasta",'../data/proteins/'+uniprot_id + ".fasta",pfam_id,uniprot_id)

                
            else:
                print("Error: File not downloaded")
                
    except:
        print(uniprot_id, " has no associated Pfam")
    


#for  protein in downloaded_pair:
    


input_file = "ALIGN.ann"


#for site_pair, score in mfdca_FN_APC[:5]:
 #   print(site_pair, score)

#print(mfdca_FN_APC)

#plmdca_visualizer = contact_visualizer.DCAVisualizer('protein', 'x', 'P26790',
#    refseq_file = ref,
#    sorted_dca_scores = plmdca_FN_APC,
#    linear_dist = 4,
#    contact_dist = 8.0,
#)

#contact_map_data = plmdca_visualizer.plot_contact_map()




