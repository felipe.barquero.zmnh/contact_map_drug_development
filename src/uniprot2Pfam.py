from prody import searchPfam
import os
import pandas as pd 
import numpy as np
import networkx as nx
from itertools import product as prod
from networkx import bipartite
from networkx.drawing.layout import _fruchterman_reingold,_process_params, rescale_layout

import matplotlib.pyplot as plt
from scipy.stats import multivariate_normal



def search_pfam(uniprot_id):
    try:
        fam = searchPfam(uniprot_id)
        return fam
    except:
        #print(f"{uniprot_id} has no associated Pfam")
        return {}

def create_pfam_graph():
    df = pd.read_csv("data/All_uniprot_ids_DrugDevelopment_project.csv")
    df['pfam_json'] = df['uniprot_id'].apply(lambda x :search_pfam(x))
    df['pfam_list'] = df['pfam_json'].apply(lambda x :[*x])
    vertex_list_prot = df.uniprot_id.unique()
    vertex_list_fam = set().union(*df.pfam_list)

    B = nx.Graph() 
    B.add_nodes_from(vertex_list_prot, bipartite=0)
    B.add_nodes_from(vertex_list_fam, bipartite=1)
    for i in range(len(df)-1):
        B.add_edges_from(list(prod([df.uniprot_id[i+1]], df.pfam_list[i+1])))

    nx.write_gpickle(B,'data/pfam_graph.pkl')
    df.to_excel('data/proteins_with_pfams.xlsx', index = False)


#create_pfam_graph()
print(os.getcwd())
B = nx.read_gpickle("contact_map_drug_development/data/pfam_graph.pkl")

### Degree Sequences


# sorted_edges = sorted(B.degree(), key=lambda x: x[1], reverse=True)

# sorted_families = [x[0] for x in sorted_edges if x[0][:2] == 'PF' ]
# sorted_proteins = [x[0] for x in sorted_edges if x[0] not in sorted_families]


# sorted_families_deg = [x[1] for x in sorted_edges if x[0][:2] == 'PF' ]
# sorted_proteins_deg = [x[1] for x in sorted_edges if x[0] not in sorted_families]

# #def plot_degree_sequence(Graph)
# fig = plt.figure("Degree Histograms", figsize=(8, 8))
# axgrid = fig.add_gridspec(2, 4)



# ax1 = fig.add_subplot(axgrid[:, :2])
# ax1.bar(*np.unique(sorted_proteins_deg, return_counts=True))
# ax1.set_title("Protein Degree histogram")
# ax1.set_xlabel("Degree")
# ax1.set_ylabel("# of Families")

# ax2 = fig.add_subplot(axgrid[:, 2:])
# ax2.bar(*np.unique(sorted_families_deg, return_counts=True))
# ax2.set_title("Family Degree histogram")
# ax2.set_xlabel("Degree")
# ax2.set_ylabel("# of Proteins")

# fig.tight_layout()


# plt.show()


all_families = pd.read_csv('contact_map_drug_development/data/pfam_query.csv')

def set_threhold(fam_size,dataset,familes,proteins,B):
    available = dataset[dataset['Number_of_proteins']<fam_size].accession.to_list()
    familes_available = set ([f for f in familes if f in available])
    #print(familes_available)
    print(len(familes)- len(familes_available), 'familes were excluded.' )
    available_proteins= set([pt for pf in familes_available  for pt in B.neighbors(pf)])
    print(len(proteins)- len(available_proteins), 'proteins were excluded.' )
    #print (available_proteins)
    print ("--------------")
    nodes = familes_available.union(available_proteins)
    #print(nodes)
    print ("--------------")
    Sub = nx.induced_subgraph(B,nodes)
    #print (list(Sub.edges))
    deleted_proteins = list(set(proteins).difference(available_proteins))

    #Sub.remove_nodes_from(nx.isolates(B))
    return Sub,deleted_proteins

def plot_histogram(graph):
    sorted_edges = sorted(graph.degree(), key=lambda x: x[1], reverse=True)
    #print (sorted_edges)

    sorted_families = [x[0] for x in sorted_edges if x[0][:2] == 'PF' ]
    sorted_proteins = [x[0] for x in sorted_edges if x[0] not in sorted_families]


    sorted_families_deg = [x[1] for x in sorted_edges if x[0][:2] == 'PF' ]
    sorted_proteins_deg = [x[1] for x in sorted_edges if x[0] not in sorted_families]

    #python
    # print (sorted_families_deg)

    #def plot_degree_sequence(Graph)
    fig = plt.figure("Degree Histograms", figsize=(8, 8))
    axgrid = fig.add_gridspec(2, 4)



    ax1 = fig.add_subplot(axgrid[:, :2])
    ax1.bar(*np.unique(sorted_proteins_deg, return_counts=True))
    ax1.set_title("Protein Degree histogram")
    ax1.set_xlabel("Degree")
    ax1.set_ylabel("# of Families")

    ax2 = fig.add_subplot(axgrid[:, 2:])
    ax2.bar(*np.unique(sorted_families_deg, return_counts=True))
    ax2.set_title("Family Degree histogram")
    ax2.set_xlabel("Degree")
    ax2.set_ylabel("# of Proteins")

    fig.tight_layout()


    plt.show()
sorted_edges = sorted(B.degree(), key=lambda x: x[1], reverse=True)

families = [x[0] for x in sorted_edges if x[0][:2] == 'PF' ]
proteins = [x[0] for x in sorted_edges if x[0] not in families]

sub,deleted = set_threhold(1300000,all_families,families,proteins,B)
#print (available)
#nx.draw_networkx(sub)

plot_histogram(sub)


bindings = pd.read_csv("contact_map_drug_development/data/Drug-Target-Binding-v3-uniprot_id.csv")
print (deleted)
print (bindings[bindings.uniprot_id.isin(deleted)].groupby('uniprot_id').size().sum(), "bindings were deleted")
print(bindings.groupby('uniprot_id').size().sum())





#print (all_families[:100])
