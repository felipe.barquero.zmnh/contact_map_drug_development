import numpy as np

def normalize_vector(v):
    """Normalize a 3D vector."""
    norm = np.linalg.norm(v)
    return v / norm

def spherical_coordinates(v,p, pi_normal, o):
    """Calculate spherical coordinates of a vector with respect to the plane pi extended with an orthogonal vector."""
    v = v - o
    v_proj = v - np.dot(v, pi_normal) * pi_normal 
    v_normal =  np.dot(v, pi_normal) * pi_normal
    p = p -o
    
  
    azimuth = np.arccos(np.dot(normalize_vector(np.cross(v,pi_normal)),normalize_vector(np.cross(p,pi_normal))))
    #azimuth = np.arccos(np.dot(p-o,v_proj)/(np.linalg.norm(v_proj)*np.linalg.norm(p-o)))
    #az_q = np.arccos(np.dot(v_proj, q-o)/(np.linalg.norm(v_proj)*np.linalg.norm(q-o)))

    #azimuth = np.arctan2(np.dot(v_proj, s-o), np.dot(v_proj, p-o))
    #elevation = np.arcsin(np.dot(v, pi_normal) / np.linalg.norm(v))
    
    elevation = np.arccos(np.dot(v,v_normal) / (np.linalg.norm(v_normal) * np.linalg.norm(v)))
  
    distance = np.linalg.norm(v)
    return azimuth, elevation, distance

def quadruplet(o,s,p,v):
    #print ('origin',o)
    #print ('plane_v1',s)
    #print ('plane_v2',p)
    #print ('vector',v)

    pi_normal = normalize_vector(np.cross(s-o, p-o))
    azimuth, elevation, distance = spherical_coordinates(v,s, pi_normal, o)
    return azimuth, elevation, distance

if __name__ == "__main__":
    # Define the vectors in 3D
    o = np.array([0, 0, 0])  # Origin
    s = np.array([1, 0, 0])  # Vector s
    p = np.array([0, 1, 0])  # Vector p
    v = np.array([1, 1, 1])  # Vector v

    z = np.array([2,3, 4])
    o = o +z
    p = p +z
    s = s +z#
    v = v +z
    # Calculate the normal vector of the plane pi
    pi_normal = normalize_vector(np.cross(s-o, p-o))

    # Extend the plane pi with an orthogonal vector
    extended_pi_normal = normalize_vector(pi_normal + (o))

    # Calculate the spherical coordinates of vector v with respect to the extended plane
    azimuth, elevation, distance = spherical_coordinates(v,s, pi_normal, o)

    # Convert angles from radians to degrees
    azimuth_deg = np.degrees(azimuth)
    elevation_deg = np.degrees(elevation)

    # Output the spherical coordinates
    print("Azimuth (degrees):", azimuth_deg)
    print("Elevation (degrees):", elevation_deg)
    print("Distance:", distance)
