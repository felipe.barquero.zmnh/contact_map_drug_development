####
# This script takes the file uniprot_pbd.json wich contain a dictionary keys the uniprot_id and value 
# a PDB with at least 95 identity profile. 
# Then we download both Alphafold of the protein and the PDB.
# We calculate the Spherical Coordinates with respect to the closest frame of reference.
# Both Alphafold and PDB are coordinates all calculated and store as a JSON e.g P12345_coordinates.json
# This JSON will be used latter to analyse their distribution and calculate Spherical Harmonics Transfomations.
####

###
# Felipe Barquero
###


import urllib.request
from Bio import PDB
import requests
import os
from spherical_coord import spherical_coordinates,quadruplet

def get_coordinates_alphafold(pdb_id):
    url = "https://alphafold.ebi.ac.uk/files/{}.pdb".format(pdb_id)
    output_file = "../data/{}_alpha.pdb".format(pdb_id)

    if not os.path.exists(os.path.dirname(output_file)):
        os.makedirs(os.path.dirname(output_file))

    # Download the PDB file using urllib.request.urlretrieve()
    urllib.request.urlretrieve(url, output_file)
    print("Successfully downloaded PDB file for {} as {}".format(pdb_id, output_file))

    parser = PDB.PDBParser()
    structure = parser.get_structure(pdb_id, output_file)

    alpha_carbons = []
    positions = []
    for model in structure:
        for chain in model:
            for residue in chain:
                if residue.has_id('CA'):
                    alpha_carbons.append(residue['CA'].get_coord())
                    positions.append(residue.get_full_id()[3][1])  # Get the position in the original chain

    # Remove the downloaded PDB file after extracting the coordinates
    os.remove(output_file)

    return alpha_carbons, positions

    

def close_reference(list_of_keys, origin, vector):
    from itertools import combinations,permutations
    available = set(list_of_keys) - set([origin,vector]) 
    translated = [protein - origin for protein in available]
    couples =  permutations(translated,2)
    min_sum = float('inf')
    min_positions = None
    pair_ = (None,None)
    for positions, pair in enumerate(couples):
        current_sum =  abs(pair[0]) + abs(pair[1])
        if current_sum < min_sum:
            min_sum = current_sum
            min_positions = positions
            pair_ = (pair[0]+ origin, pair[1]+ origin)
            if current_sum < 3:
                return pair_

            
    return pair_

def get_coordinates(pdb_id):
    # Download the PDB file
    pdb_url = "https://files.rcsb.org/download/{}.pdb".format(pdb_id)
    pdb_file_path = "{}.pdb".format(pdb_id)
    urllib.request.urlretrieve(pdb_url, pdb_file_path)

    # Parse the PDB file and extract the alpha carbon coordinates
    parser = PDB.PDBParser()
    structure = parser.get_structure(pdb_id, pdb_file_path)
 

    alpha_carbons = []
    positions = []
    for model in structure:
        for chain in model:
            for residue in chain:
                if residue.has_id('CA'):
                    alpha_carbons.append(residue['CA'].get_coord())
                    positions.append(residue.get_full_id()[3][1])  # Get the position in the original chain

    # Remove the downloaded PDB file after extracting the coordinates
    import os
    os.remove(pdb_file_path)

    return alpha_carbons, positions
import json
from collections import OrderedDict

#coordinates = json.load(open('uniprot_pdb.json'), object_pairs_hook=OrderedDict)

if __name__ == "__main__":
    proteins = json.load(open('uniprot_pbd.json'), object_pairs_hook=OrderedDict)
    for prot in proteins:

        import numpy as np
        pdb_id = proteins[prot].split("_")[0]  # Replace this with the PDB ID of the structure you want to retrieve
        print(pdb_id)

        
        try:
            
            alpha_carbons, positions = get_coordinates(pdb_id)
            print("Number of alpha carbons:", len(alpha_carbons))
            print("Amino Acid Positions and Coordinates:")
        
            coord_R = {positions[i]: np.array(alpha_carbons[i]) for i in range(len(positions)) }

            pdb_id = "AF-"+ prot +"-F1-model_v4" 
            print (pdb_id)
            alpha_carbons, positions = get_coordinates_alphafold(pdb_id)
        
            coord_P = {positions[i]: np.array(alpha_carbons[i]) for i in range(len(positions)) }
            
            
        
            

            from itertools import combinations,permutations
            
            list_of_distances = []
            for tup in permutations(coord_R.keys(),2):
                quad = [tup[0],tup[1],None,None]
                (quad[2],quad[3]) =  close_reference(coord_R.keys(),quad[0],quad[1])
                print((quad[0],quad[1]),(quad[2],quad[3]))
                dictionario = {}
                dictionario_predicted = {}
                azimuth, elevation, distance = quadruplet(coord_P[quad[0]],coord_P[quad[2]],coord_P[quad[3]],coord_P[quad[1]]) 
                azimuth_deg = np.degrees(azimuth)
                elevation_deg = np.degrees(elevation)
            
                dictionario_predicted['Azimuth'] = str(azimuth)
                dictionario_predicted['Elevation'] = str(elevation)
                dictionario_predicted['Azimuth (degrees)'] = str(azimuth_deg)
                dictionario_predicted['Elevation (degrees):'] = str(elevation_deg)
                dictionario_predicted['Distance:'] = str(distance)
                

        
                dictionario_real = {}
                azimuth, elevation, distance = quadruplet(coord_R[quad[0]],coord_R[quad[2]],coord_R[quad[3]],coord_R[quad[1]]) 
                azimuth_deg = np.degrees(azimuth)
                elevation_deg = np.degrees(elevation)




                # Output the spherical coordinates
            
            

                dictionario_real['Azimuth'] =str(azimuth)
                dictionario_real['Elevation'] = str(elevation)
                dictionario_real['Azimuth (degrees)'] = str(azimuth_deg)
                dictionario_real['Elevation (degrees):'] = str(elevation_deg)
                dictionario_real['Distance:'] = str(distance)
                dictionario = {'Real': dictionario_real, 'Predicted': dictionario_predicted,
                                'Origin':str(quad[0]), 'Plane vector 1': str(quad[2]), 'Plane vector 2': str(quad[3]), 'Vector': str(quad[1])}
                list_of_distances.append(dictionario)
                

            import json
            with open('data/spherical_coordinates/'+ prot+'_coordinates.json', 'w') as json_file:
                json.dump(list_of_distances, json_file)#
        except:
            print(prot,'could not be retrieved')

    # Convert angles from radians to degrees
    azimuth_deg = np.degrees(azimuth)
    elevation_deg = np.degrees(elevation)

    # Output the spherical coordinates
    print("Azimuth (degrees):", azimuth_deg)
    print("Elevation (degrees):", elevation_deg)
    print("Distance:", distance)

    
