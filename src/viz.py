import numpy as np
import seaborn as sns
import matplotlib.pyplot as plt
from functools import partial
import os 
from mpl_toolkits.mplot3d import Axes3D
from numpy import linalg
import json
import seaborn as sns
from collections import OrderedDict
import pandas as pd


def savefig(figures, *args, **kwargs):
    if "FIGDIR" in os.environ:
        figdir = os.environ["FIGDIR"]
        for name, figure in figures.items():
            fname_full = os.path.join(figdir, name)
            #print(f"saving image to {fname_full}")
            figure.savefig(f"{fname_full}.pdf", *args, **kwargs)
            figure.savefig(f"{fname_full}.png", *args, **kwargs)


def scale_3d(ax, x_scale, y_scale, z_scale, factor):    
    scale=np.diag([x_scale, y_scale, z_scale, 1.0])    
    scale=scale*(1.0/scale.max())    
    scale[3,3]=factor
    def short_proj():    
        return np.dot(Axes3D.get_proj(ax), scale)    
    return short_proj    

def style3d(ax, x_scale, y_scale, z_scale, factor=0.62):
    plt.gca().patch.set_facecolor('white')
    ax.w_xaxis.set_pane_color((0, 0, 0, 0))
    ax.w_yaxis.set_pane_color((0, 0, 0, 0))
    ax.w_zaxis.set_pane_color((0, 0, 0, 0))
    ax.get_proj = scale_3d(ax, x_scale, y_scale, z_scale, factor)


def plot_3d_belief_state(mu_hist, dim, ax, skip=3, npoints=2000, azimuth=-30, elevation=30, h=0.5):
    nsteps = len(mu_hist)
    xmin, xmax = mu_hist[..., dim].min(), mu_hist[..., dim].max()
    xrange = np.linspace(xmin, xmax, npoints).reshape(-1, 1)
    res = np.apply_along_axis(lambda X: kdeg(xrange, X[..., None], h), 1, mu_hist)
    densities = res[..., dim]
    for t in range(0, nsteps, skip):
        tloc = t * np.ones(npoints)
        px = densities[t]
        ax.plot(tloc, xrange, px, c="tab:blue", linewidth=1)
    ax.set_zlim(0, 1)
    style3d(ax, 1.8, 1.2, 0.7, 0.8)
    ax.view_init(elevation, azimuth)
    ax.set_xlabel(r"$t$", fontsize=13)
    ax.set_ylabel(r"$x_{"f"d={dim}"",t}$", fontsize=13)
    ax.set_zlabel(r"$p(x_{d, t} \vert y_{1:t})$", fontsize=13)

coordinates = json.load(open('data_small.json'), object_pairs_hook=OrderedDict)
df = pd.DataFrame(coordinates)

def sum_columns_ea(row):
    return -np.log( (float(row['Real']['Azimuth'])-float(row['Predicted']['Azimuth']))**2 + (float(row['Real']['Elevation'])-float(row['Predicted']['Elevation']))**2)

df['RSM_ea']= df.apply(sum_columns_ea, axis=1)

from mpl_toolkits.mplot3d import Axes3D
import matplotlib.pyplot as plt
import numpy as np

fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')
nbins = 50

ys = df['RSM_ea']
data_dict = {
     np.random.random() : ys.to_list() for i in range(15)}
print (data_dict)
# Create a 3D subplot
fig = plt.figure()
ax = fig.add_subplot(111, projection='3d')

# Loop through dictionary keys and values
for key, values in data_dict.items():

    # Create a KDE plot for each key's values
    sns.kdeplot(data=ys ,color = 'blue',ax=ax, zdir='y',zs=key)

# Set labels for the axes
ax.set_xlabel('X Label')
ax.set_ylabel('Key')
ax.set_zlabel('Density')

# Set the title of the plot
ax.set_title('3D KDE Plot with Varying Key')

# Show the plot

plt.show()